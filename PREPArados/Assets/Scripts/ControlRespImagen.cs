﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlRespImagen : MonoBehaviour {
	
	public Text ZonaPregunta;
	public Image Opcion1;
	public Image Opcion2;
	public Image Opcion3;
	public Image Opcion4;
	
	public RespuestaImg[] PregObjetos;
	private int Num;
	private int DetenerTiempo;
	
	public Animator IncorrectoAnim;
	public Animator CorrectoAnim;
	public Image ExplImg;
	
	public Text TimerText;
	private float Timer;
	
	public Text NumeroDePregunta;

	// Use this for initialization
	void Start () {
		NumeroDePregunta.text = ("" +(PlayerPrefs.GetInt ("PreguntaMAT2") + PlayerPrefs.GetInt ("PreguntaMAT") + 1)+"75");
		if(PlayerPrefs.GetInt ("PreguntaMAT2") < PregObjetos.Length)
			Num = PlayerPrefs.GetInt ("PreguntaMAT2");
		else
			Application.LoadLevel ("MatePregImg");

		DetenerTiempo = 0;
		Debug.Log ("" + Num);
		Timer = 90;
		LLenarDatos();
		ExplImg.sprite = PregObjetos [Num].GetExplicacion ();
		ExplImg.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		TimerText.text = ("" + (int)Timer);
		
		
		if (Timer <= 0) {
			IncorrectoAnim.Play ("Incorrecto");
			ExplImg.enabled = true;
			PlayerPrefs.SetInt ("PreguntaMAT2", PlayerPrefs.GetInt ("PreguntaMAT2") +1);
		} else {
			if(DetenerTiempo == 0)
				Timer -= Time.deltaTime;
		}
	}
	
	public void LLenarDatos(){
		ZonaPregunta.text = PregObjetos[Num].GetPregunta();
		Opcion1.sprite = PregObjetos [Num].GetRespuesta1 ();
		Opcion2.sprite = PregObjetos [Num].GetRespuesta2 ();
		Opcion3.sprite = PregObjetos [Num].GetRespuesta3 ();
		Opcion4.sprite = PregObjetos [Num].GetRespuesta4 ();
		//Opcion5.text = ("" + PregObjetos[Num].GetRespuesta5());
	}
	
	public void Elegir(int resp){
		if (resp == PregObjetos [Num].GetCorrecta ()) {
			CorrectoAnim.Play ("correcto");
			PlayerPrefs.SetInt ("ScoreMAT", PlayerPrefs.GetInt("ScoreMAT") +1 );
		} else {
			IncorrectoAnim.Play ("Incorrecto");
			ExplImg.enabled = true;
		}
		DetenerTiempo = 1;
		PlayerPrefs.SetInt ("PreguntaMAT2", PlayerPrefs.GetInt ("PreguntaMAT2") +1);
	}
	
	public void Continuar(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void RegresarMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
