﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlPregImagen : MonoBehaviour {
	
	public Text ZonaPregunta;
	public Image ZonaImagen;
	public Text Opcion1;
	public Text Opcion2;
	public Text Opcion3;
	public Text Opcion4;
	//public Text Opcion5;
	
	public PreguntaImg[] PregObjetos;
	private int Num;
	private int DetenerTiempo;
	
	public Animator IncorrectoAnim;
	public Animator CorrectoAnim;
	public Text ExplText;
	
	public Text TimerText;
	private float Timer;
	public Text NumeroDePregunta;

	// Use this for initialization
	void Start () {
		NumeroDePregunta.text = ("" +(PlayerPrefs.GetInt ("PreguntaMAT3") + PlayerPrefs.GetInt ("PreguntaMAT2") + PlayerPrefs.GetInt ("PreguntaMAT") + 1)+"/75");
		if(PlayerPrefs.GetInt ("PreguntaMAT3") < PregObjetos.Length)
			Num = PlayerPrefs.GetInt ("PreguntaMAT3");
		else
			Application.LoadLevel ("ScoreMATFinal");

		DetenerTiempo = 0;
		Debug.Log ("" + Num);
		Timer = 90;
		LLenarDatos();
	}
	
	// Update is called once per frame
	void Update () {
		TimerText.text = ("" + (int)Timer);
		
		
		if (Timer <= 0) {
			IncorrectoAnim.Play ("Incorrecto");
			ExplText.text = ("" + PregObjetos [Num].GetExplicacion ());
			PlayerPrefs.SetInt ("PreguntaMAT3", PlayerPrefs.GetInt ("PreguntaMAT3") +1);
		} else {
			if(DetenerTiempo == 0)
				Timer -= Time.deltaTime;
		}
	}
	
	public void LLenarDatos(){
		ZonaPregunta.text = PregObjetos[Num].GetPregunta();
		ZonaImagen.sprite = PregObjetos [Num].GetImage ();
		Opcion1.text = ("" + PregObjetos[Num].GetRespuesta1());
		Opcion2.text = ("" + PregObjetos[Num].GetRespuesta2());
		Opcion3.text = ("" + PregObjetos[Num].GetRespuesta3());
		Opcion4.text = ("" + PregObjetos[Num].GetRespuesta4());
		//Opcion5.text = ("" + PregObjetos[Num].GetRespuesta5());
	}
	
	public void Elegir(int resp){
		if (resp == PregObjetos [Num].GetCorrecta ()) {
			CorrectoAnim.Play ("correcto");
			PlayerPrefs.SetInt ("ScoreMAT", PlayerPrefs.GetInt("ScoreMAT") +1 );
		} else {
			IncorrectoAnim.Play ("Incorrecto");
			ExplText.text = (""+ PregObjetos[Num].GetExplicacion());
		}
		
		DetenerTiempo = 1;
		PlayerPrefs.SetInt ("PreguntaMAT3", PlayerPrefs.GetInt ("PreguntaMAT3") +1);
	}
	
	public void Continuar(){
		Application.LoadLevel (Application.loadedLevel);
	}
	
	public void RegresarMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
