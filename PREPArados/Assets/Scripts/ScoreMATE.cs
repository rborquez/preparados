﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreMATE : MonoBehaviour {
	
	public Text ScoreText;
	public int NumPreguntas;//cuantas preguntas tiene la materia
	
	private int FinalScore;
	private int score;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("PreguntaMAT", 0);
		PlayerPrefs.SetInt ("PreguntaMAT2", 0);
		PlayerPrefs.SetInt ("PreguntaMAT3", 0);
		score = PlayerPrefs.GetInt ("ScoreMAT");
		FinalScore =  (score*100)/NumPreguntas ;
		Debug.Log (score);
		Debug.Log (FinalScore);
		ScoreText.text = ("" + FinalScore);

		if (FinalScore > PlayerPrefs.GetInt ("BestScoreMAT")) {
			PlayerPrefs.SetInt ("BestScoreMAT", FinalScore);
		}

		PlayerPrefs.SetInt ("ScoreMAT", 0);
	}
	
	
	public void ReturnToMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
