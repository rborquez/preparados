﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlDePreguntas : MonoBehaviour {

	public string IDMateria;
	public Text ZonaPregunta;
	public Text Opcion1;
	public Text Opcion2;
	public Text Opcion3;
	public Text Opcion4;
	//public Text Opcion5;

	public PreguntaObjeto[] PregObjetos;
	private int Num;
	private int DetenerTiempo;

	public Animator IncorrectoAnim;
	public Animator CorrectoAnim;
	public Animator MostrarExpl;
	public Text ExplText;

	public Text TimerText;
	private float Timer;
	public Text NumeroDePregunta;

	void Start () {
		if(IDMateria == "MAT")
			NumeroDePregunta.text = ("" +(PlayerPrefs.GetInt ("Pregunta" + IDMateria) +1)+"/75");
		else 
			NumeroDePregunta.text = ("" +(PlayerPrefs.GetInt ("Pregunta" + IDMateria) +1)+"/" +PregObjetos.Length);

		if(PlayerPrefs.GetInt ("Pregunta" + IDMateria) < PregObjetos.Length)
			Num = PlayerPrefs.GetInt ("Pregunta" + IDMateria);
		else
			Application.LoadLevel ("Score"+IDMateria);

		DetenerTiempo = 0;
		Debug.Log ("" + Num);

		if (IDMateria == "MAT")
			Timer = 90;
		else
			Timer = 30;

		LLenarDatos();
	}
	
	// Update is called once per frame
	void Update () {
		TimerText.text = ("" + (int)Timer);


		if (Timer <= 0) {
			IncorrectoAnim.Play ("Incorrecto");
			MostrarExpl.Play ("ExplicacionMostrar");
			ExplText.text = ("" + PregObjetos [Num].GetExplicacion ());
			PlayerPrefs.SetInt ("Pregunta" + IDMateria, PlayerPrefs.GetInt ("Pregunta" + IDMateria) +1);
		} else {
			if(DetenerTiempo == 0)
				Timer -= Time.deltaTime;
		}
	}

	public void LLenarDatos(){
		ZonaPregunta.text = PregObjetos[Num].GetPregunta();
		Opcion1.text = ("" + PregObjetos[Num].GetRespuesta1());
		Opcion2.text = ("" + PregObjetos[Num].GetRespuesta2());
		Opcion3.text = ("" + PregObjetos[Num].GetRespuesta3());
		Opcion4.text = ("" + PregObjetos[Num].GetRespuesta4());
		//Opcion5.text = ("" + PregObjetos[Num].GetRespuesta5());
	}

	public void Elegir(int resp){
		if (resp == PregObjetos [Num].GetCorrecta ()) {
			CorrectoAnim.Play ("correcto");
			PlayerPrefs.SetInt ("Score"+IDMateria, PlayerPrefs.GetInt("Score"+IDMateria) +1 );
		}
		else
			IncorrectoAnim.Play("Incorrecto");

		DetenerTiempo = 1;
		MostrarExpl.Play ("ExplicacionMostrar");
		ExplText.text = (""+ PregObjetos[Num].GetExplicacion());
		PlayerPrefs.SetInt ("Pregunta" + IDMateria, PlayerPrefs.GetInt ("Pregunta" + IDMateria) +1);
	}

	public void Continuar(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void RegresarMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
