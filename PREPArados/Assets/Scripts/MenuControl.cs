﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuControl : MonoBehaviour {

	public Text HMBest;
	public Text HUBest;
	public Text MATBest;
	public Text LITBest;
	
	void Start () {
		HUBest.text = (": " + PlayerPrefs.GetInt ("BestScoreHU"));
		HMBest.text = (": " + PlayerPrefs.GetInt ("BestScoreHM"));
		MATBest.text = (": " + PlayerPrefs.GetInt ("BestScoreMAT"));
		LITBest.text = (": " + PlayerPrefs.GetInt ("BestScoreLIT"));
	}

	public void HistUniv(){
		//PlayerPrefs.SetInt ("IDHU", 0);
		Application.LoadLevel ("HistoriaUniv");
	}

	public void HistMex(){
		Application.LoadLevel ("HistoriaMex");
	}

	public void Mate(){
		Application.LoadLevel ("Matematicas");
	}

	public void Liter(){
		Application.LoadLevel ("Literatura");
	}

	public void Menu(){
		Application.LoadLevel ("Menu");
	}
}
