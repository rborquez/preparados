﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RespuestaImg : MonoBehaviour {
	
	public string Pregunta;
	public Sprite Respuesta1;
	public Sprite Respuesta2;
	public Sprite Respuesta3;
	public Sprite Respuesta4;
	public int RespCorrecta;
	public Sprite Explicacion;
	
	public string GetPregunta(){
		return Pregunta;
	}
	
	public Sprite GetRespuesta1(){
		return Respuesta1;
	}
	
	public Sprite GetRespuesta2(){
		return Respuesta2;
	}
	
	public Sprite GetRespuesta3(){
		return Respuesta3;
	}
	
	public Sprite GetRespuesta4(){
		return Respuesta4;
	}
	
	public int GetCorrecta(){
		return RespCorrecta;
	}
	public Sprite GetExplicacion(){
		return Explicacion;
	}
}