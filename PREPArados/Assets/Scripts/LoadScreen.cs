﻿using UnityEngine;
using System.Collections;

public class LoadScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (GoToGame());
	}
	
	private IEnumerator GoToGame(){
		yield return new WaitForSeconds (3);
		Application.LoadLevel("Menu");
	}
}
