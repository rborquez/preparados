﻿using UnityEngine;
using System.Collections;

public class PreguntaObjeto : MonoBehaviour {

	public string Pregunta;
	public string Respuesta1;
	public string Respuesta2;
	public string Respuesta3;
	public string Respuesta4;
	public string Respuesta5;
	public int RespCorrecta;
	public string Explicacion;

	public string GetPregunta(){
		return Pregunta;
	}
	
	public string GetRespuesta1(){
		return Respuesta1;
	}

	public string GetRespuesta2(){
		return Respuesta2;
	}

	public string GetRespuesta3(){
		return Respuesta3;
	}

	public string GetRespuesta4(){
		return Respuesta4;
	}

	public string GetRespuesta5(){
		return Respuesta5;
	}

	public int GetCorrecta(){
		return RespCorrecta;
	}
	public string GetExplicacion(){
		return Explicacion;
	}
}
