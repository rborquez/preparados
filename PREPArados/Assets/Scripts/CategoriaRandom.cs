﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CategoriaRandom : MonoBehaviour {

	public Text indicador;
	public GameObject[] pelotas;
	public GameObject[] pelotas2;
	public GameObject tope;

	public GameObject[] ballPrefab;
	public Vector3 spawningPosition;
	private int SelectedBall;
	GameObject spawnedBall =null;

	void Start(){
		SelectedBall = Random.Range (0,4);
		spawningPosition = new Vector3 (0.03f, -1.38f, 0);
		spawnedBall = Instantiate (ballPrefab[SelectedBall], spawningPosition, Quaternion.identity) as GameObject;
	}

	public void reload(){
		//Application.LoadLevel("Ruleta");
		StartCoroutine (SelectCategory());
		StartCoroutine (SelectCategory2());
	}

	private IEnumerator SelectCategory(){
		for (int i=0; i<pelotas.Length; i++) {
			pelotas [i].GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, 1) * 350.0f);
			yield return new WaitForSeconds (0.1f);
		}
		for (int i=0; i<pelotas.Length; i++) {
			pelotas [i].GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, 1) * 350.0f);
			yield return new WaitForSeconds (0.1f);
		}
		StartCoroutine (Seleccion());
	}
	private IEnumerator SelectCategory2(){
		for (int i=0; i<pelotas.Length; i++) {
			pelotas2 [i].GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, 1) * 350.0f);
			yield return new WaitForSeconds (0.1f);
		}
		for (int i=0; i<pelotas.Length; i++) {
			pelotas2 [i].GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, 1) * 350.0f);
			yield return new WaitForSeconds (0.1f);
		}
	}


	private IEnumerator Seleccion(){
		yield return new WaitForSeconds (0.4f);
		Destroy (tope);
		yield return new WaitForSeconds (2);
		GoToGame ();
	}

	public void GoToGame(){
		switch (SelectedBall) {
		case 0:
			Application.LoadLevel("RandMatematicas");
			break;
		case 1:
			Application.LoadLevel("RandHistoriaMex");
			break;
		case 2:
			Application.LoadLevel("RandHistoriaUniv");
			break;
		case 3:
			Application.LoadLevel("RandLiteratura");
			break;

		}
	}


}
