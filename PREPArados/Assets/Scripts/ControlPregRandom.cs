﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlPregRandom : MonoBehaviour {

	public string IDMateria;
	public Text ZonaPregunta;
	public Text Opcion1;
	public Text Opcion2;
	public Text Opcion3;
	public Text Opcion4;
	
	public PreguntaObjeto[] PregObjetos;
	private int Num;
	private int DetenerTiempo;
	
	public Animator IncorrectoAnim;
	public Animator CorrectoAnim;
	//public Animator MostrarExpl;//explicacion
	public Text ExplText;
	
	public Text TimerText;
	private float Timer;
	
	void Start () {

		Num = Random.Range (0,PregObjetos.Length);
		
		DetenerTiempo = 0;
		Debug.Log ("" + Num);
		
		if (IDMateria == "MAT")
			Timer = 90;
		else
			Timer = 30;
		
		LLenarDatos();
	}
	
	// Update is called once per frame
	void Update () {
		TimerText.text = ("" + (int)Timer);
		
		
		if (Timer <= 0) {
			IncorrectoAnim.Play ("Incorrecto");
			//MostrarExpl.Play ("ExplicacionMostrar");
			ExplText.text = ("" + PregObjetos [Num].GetExplicacion ());
		} else {
			if(DetenerTiempo == 0)
				Timer -= Time.deltaTime;
		}
	}
	
	public void LLenarDatos(){
		ZonaPregunta.text = PregObjetos[Num].GetPregunta();
		Opcion1.text = ("" + PregObjetos[Num].GetRespuesta1());
		Opcion2.text = ("" + PregObjetos[Num].GetRespuesta2());
		Opcion3.text = ("" + PregObjetos[Num].GetRespuesta3());
		Opcion4.text = ("" + PregObjetos[Num].GetRespuesta4());
		//Opcion5.text = ("" + PregObjetos[Num].GetRespuesta5());
	}
	
	public void Elegir(int resp){
		if (resp == PregObjetos [Num].GetCorrecta ()) {
			CorrectoAnim.Play ("correcto");
		}
		else
			IncorrectoAnim.Play("Incorrecto");
		
		DetenerTiempo = 1;
		//MostrarExpl.Play ("ExplicacionMostrar");
		ExplText.text = (""+ PregObjetos[Num].GetExplicacion());
	}
	
	public void Continuar(){
		Application.LoadLevel ("Ruleta");
	}

}
