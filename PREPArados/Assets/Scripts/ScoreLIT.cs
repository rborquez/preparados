﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreLIT : MonoBehaviour {

	public Text ScoreText;
	public int NumPreguntas;//cuantas preguntas tiene la materia
	public string IDM;

	private int FinalScore;
	private int score;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("Pregunta"+IDM, 0);
		score = PlayerPrefs.GetInt ("Score"+IDM);
		FinalScore =  (score*100)/NumPreguntas ;
		Debug.Log (score);
		Debug.Log (FinalScore);
		ScoreText.text = ("" + FinalScore);

		if (FinalScore > PlayerPrefs.GetInt ("BestScore"+IDM)) {
			PlayerPrefs.SetInt ("BestScore"+IDM , FinalScore);
		}

		PlayerPrefs.SetInt ("Score"+IDM, 0);
	}


	public void ReturnToMenu(){
		Application.LoadLevel ("MainMenu");
	}
}
